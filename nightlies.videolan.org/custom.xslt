<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:variable name="codefile" select="document(concat( $root, $path, '/HEADER.html'),/)"/>
  <xsl:template match="/">

    <html>
      <head><title>Index of <xsl:value-of select="$path"/></title></head>
      <link rel="stylesheet" type="text/css" href="/style.css" />
      <body>
        <xsl:copy-of select="$codefile/*"/>
        <h1>Index of <xsl:value-of select="$path"/></h1><hr/>
        <table border="0">
          <tr>
            <td><a href="../">../</a></td>
          </tr>

          <xsl:for-each select="list/*">
            <xsl:sort order="descending" select="@mtime"/>

            <xsl:variable name="name">
              <xsl:choose>
                <xsl:when test="local-name(.) = 'directory'"><xsl:value-of select="."/>/</xsl:when>
                <xsl:when test="local-name(.) = 'file'"><xsl:value-of select="."/></xsl:when>
              </xsl:choose>
            </xsl:variable>

            <xsl:variable name="size">
              <xsl:if test="string-length(@size) &gt; 0">
                <xsl:if test="number(@size) &gt; 0">
                  <xsl:choose>
                    <xsl:when test="round(@size div 1024) &lt; 1"><xsl:value-of select="@size" /></xsl:when>
                    <xsl:when test="round(@size div 1048576) &lt; 1"><xsl:value-of select="format-number((@size div 1024), '0.0')" />K</xsl:when>
                    <xsl:otherwise><xsl:value-of select="format-number((@size div 1048576), '0')" />M</xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
              </xsl:if>
            </xsl:variable>

            <xsl:variable name="date">
              <xsl:variable name="mmm" select="substring(@mtime,6,2)" />
              <xsl:variable name="month" >
                <xsl:choose>
                  <xsl:when test="$mmm = '01'">Jan</xsl:when>
                  <xsl:when test="$mmm = '02'">Feb</xsl:when>
                  <xsl:when test="$mmm = '03'">Mar</xsl:when>
                  <xsl:when test="$mmm = '04'">Apr</xsl:when>
                  <xsl:when test="$mmm = '05'">May</xsl:when>
                  <xsl:when test="$mmm = '06'">Jun</xsl:when>
                  <xsl:when test="$mmm = '07'">Jul</xsl:when>
                  <xsl:when test="$mmm = '08'">Aug</xsl:when>
                  <xsl:when test="$mmm = '09'">Sep</xsl:when>
                  <xsl:when test="$mmm = '10'">Oct</xsl:when>
                  <xsl:when test="$mmm = '11'">Nov</xsl:when>
                  <xsl:when test="$mmm = '12'">Dec</xsl:when>
                </xsl:choose>
              </xsl:variable>
              <xsl:value-of select="substring(@mtime,9,2)"/>-<xsl:value-of select="$month"/>-<xsl:value-of select="substring(@mtime,1,4)"/><xsl:text> </xsl:text>
              <xsl:value-of select="substring(@mtime,12,2)"/>:<xsl:value-of select="substring(@mtime,15,2)"/>
            </xsl:variable>

            <tr>
              <td><a href="{$name}"><xsl:value-of select="$name"/></a></td>
              <td align="right"><xsl:value-of select="$size"/></td>
              <td><xsl:value-of select="$date"/></td>
            </tr>

          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
